﻿using CoolGames.Controllers;
using CoolGames.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoolGamesTests.Controllers
{
    public class JeuControllerDoit
    {
        /// <summary>
        //    /// Vérifie si la méthode Ajouter renvoie bien une vue
        //    /// </summary>
        [Fact]
        public void TestCreerJeu_Get()
        {
            DbContextOptions<GamesDbContext> dbContextOptions =
                 new DbContextOptionsBuilder<GamesDbContext>().UseInMemoryDatabase("GamesDbTest")
                 .Options;

            var _dbContext = new GamesDbContext(dbContextOptions);

            BDJeuRepository _jeuRepository = new BDJeuRepository(_dbContext);
            JeuController jeuController = new JeuController(_jeuRepository);

            var resultat = jeuController.Ajouter();

            Assert.IsType<ViewResult>(resultat);
        }
        /// <summary>
        /// Vérifie si la méthode Supprimer  supprime correctement un jeu de la base de données.
        /// </summary>
        [Fact]
        public void TestSupprimerJeu()
        {
            DbContextOptions<GamesDbContext> dbContextOptions =
                new DbContextOptionsBuilder<GamesDbContext>().UseInMemoryDatabase("GamesDbTest")
                .Options;

            var _dbContext = new GamesDbContext(dbContextOptions);

            _dbContext.Jeus.Add(
               new Jeu
               {
                   Nom = "Horizon Forbidden West",
                   DateSortie = new DateTime(2022, 2, 18),
                   PlatesFormes = "PlayStation 5",
                   Developpeur = "Guerrilla Games",
                   Editeur = "Sony Interactive Entertainment",
                   Genres = "Action-RPG",
                   Prix = 69.99,
                   Quantite = 10,
                   UrlImage = "Horizon.png"

               });
            BDJeuRepository _jeuRepository = new BDJeuRepository(_dbContext);
            JeuController jeuController = new JeuController(_jeuRepository);

            var resultat = jeuController.Supprimer(1);

            Assert.IsType<RedirectToActionResult>(resultat);
            Assert.Equal(0, _jeuRepository.ListeJeux.Count());
        }
        /// <summary>
        /// Vérifie si la méthode ValiderResult renvoie le bon message d'erreur
        /// </summary>
        [Fact]
        public void TestValiderResultat()
        {
            DbContextOptions<GamesDbContext> dbContextOptions =
                new DbContextOptionsBuilder<GamesDbContext>().UseInMemoryDatabase("GamesDbTest")
                .Options;

            var _dbContext = new GamesDbContext(dbContextOptions);

            _dbContext.Jeus.Add(
                new Jeu
                {
                    Nom = "Returnal",
                    DateSortie = new DateTime(2021, 4, 30),
                    PlatesFormes = "PlayStation 5",
                    Developpeur = "Housemarque",
                    Editeur = "Sony Interactive Entertainment",
                    Genres = "Action-Roguelike",
                    Prix = 69.99,
                    Quantite = 5,
                    UrlImage = "ReturnalUni.jpg"
                });
            _dbContext.SaveChanges();
            BDJeuRepository _jeuRepository = new BDJeuRepository(_dbContext);
            JeuController jeuController = new JeuController(_jeuRepository);
            var quantite = 5;
            var result = jeuController.ValiderResultat(quantite);

            Assert.Equal($"La quantité {quantite} existe déjà", result.Value);

        }
    }
}

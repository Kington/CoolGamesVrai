﻿using CoolGames.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoolGamesTests.Models
{
    public class BDPanierRepositoryDoit
    {
        [Fact]
        public void TestAjouterPanier()
        {
            DbContextOptions<GamesDbContext> dbContextOptions =
                new DbContextOptionsBuilder<GamesDbContext>().UseInMemoryDatabase("GamesDbTest")
                .Options;

            var _dbContext = new GamesDbContext(dbContextOptions);
            var jeu = new Jeu
            {
                Id = 1,
                Nom = "Returnal",
                DateSortie = new DateTime(2021, 4, 30),
                PlatesFormes = "PlayStation 5",
                Developpeur = "Housemarque",
                Editeur = "Sony Interactive Entertainment",
                Genres = "Action-Roguelike",
                Prix = 69.99,
                Quantite = 5,
                UrlImage = "ReturnalUni.jpg"
            };
            BDPanierRepository _panierRepository = new BDPanierRepository(_dbContext);
            _panierRepository.SetSessionId("session_id");
            _panierRepository.AjouterAuPanier(jeu);
            var panier = _dbContext.Paniers.FirstOrDefault();
            Assert.NotNull(panier);
            Assert.Equal(1, panier.Quantite);
            Assert.Equal(jeu.Id, panier.JeuId);
        }

        [Fact]
        public void TestSupprimerPanier()
        {
            DbContextOptions<GamesDbContext> dbContextOptions =
               new DbContextOptionsBuilder<GamesDbContext>().UseInMemoryDatabase("GamesDbTest")
               .Options;

            var _dbContext = new GamesDbContext(dbContextOptions);
            var jeu = new Jeu
            {
                Id = 1,
                Nom = "Returnal",
                DateSortie = new DateTime(2021, 4, 30),
                PlatesFormes = "PlayStation 5",
                Developpeur = "Housemarque",
                Editeur = "Sony Interactive Entertainment",
                Genres = "Action-Roguelike",
                Prix = 69.99,
                Quantite = 5,
                UrlImage = "ReturnalUni.jpg"
            };
            BDPanierRepository _panierRepository = new BDPanierRepository(_dbContext);
            _panierRepository.SetSessionId("session_id");
            _panierRepository.SupprimerDuPanier(jeu);
            var panier = _dbContext.Paniers.FirstOrDefault();
            Assert.Null(panier);
        }
    }
}

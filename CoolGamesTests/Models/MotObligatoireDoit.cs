﻿using CoolGames.AttributsPersonnalises;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoolGamesTests.Models
{
    public class MotObligatoireDoit
    {
        [Fact]
        public void TestMotObligatoire()
        {
            var attribute = new MotObligatoireAttribute("Playstation 5", "Xbox Series", "Switch");
            var validationContext = new ValidationContext(new { Platforme = "Playstation 5" });

            // Act
            var result = attribute.GetValidationResult("Playstation 5", validationContext);

            // Assert
            Assert.Null(result);
        }
    }
}

﻿using CoolGames.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoolGamesTests.Models
{
    public class BDJeuRepositoryDoit
    {
        /// <summary>
        //    /// Vérifie si la méthode Recherche revoit le nom associé au string
        //    /// </summary>
        [Fact]
        public void TestRechercheJeu()
        {
            DbContextOptions<GamesDbContext> dbContextOptions =
                new DbContextOptionsBuilder<GamesDbContext>().UseInMemoryDatabase("GamesDbTest")
                .Options;

            var _dbContext = new GamesDbContext(dbContextOptions);

            _dbContext.Jeus.AddRange(
                new Jeu
                {
                    Nom = "Ratchet & Clank: Rift Apart",
                    DateSortie = new DateTime(2021, 6, 11),
                    PlatesFormes = "PlayStation 5",
                    Developpeur = "Insomniac Games",
                    Editeur = "Sony Interactive Entertainment",
                    Genres = "Action-Platformer",
                    Prix = 79.99,
                    Quantite = 8,

                    UrlImage = "Ratchet.png"
                },
                new Jeu
                {
                    Nom = "Returnal",
                    DateSortie = new DateTime(2021, 4, 30),
                    PlatesFormes = "PlayStation 5",
                    Developpeur = "Housemarque",
                    Editeur = "Sony Interactive Entertainment",
                    Genres = "Action-Roguelike",
                    Prix = 69.99,
                    Quantite = 5,
                    UrlImage = "ReturnalUni.jpg"
                });
            _dbContext.SaveChanges();
            BDJeuRepository _jeuRepository = new BDJeuRepository(_dbContext);

            var cherche = _jeuRepository.Recherche("Rat");
            var chercheDeux = _jeuRepository.Recherche("Returnal");

            Assert.Contains("Returnal", chercheDeux.Select(j => j.Nom));
            Assert.Contains("Ratchet & Clank: Rift Apart", cherche.Select(j => j.Nom));
            Assert.Single(cherche);
            Assert.Single(chercheDeux);
        }
    }
}

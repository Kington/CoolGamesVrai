﻿using CoolGames.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoolGamesTests.Models
{
    // TODO: Belle progression dans les tests unitaires, bravo!
    public class ConsoleJeuDoit
    {
        /// <summary>
        //    /// Vérifie si la validation est correct donc si elle retourne true
        //    /// J'aurais pu créer 2 méthode pour tester un true et false mais je l'ai mis dans 1 donc il vaut 2
        //    /// </summary>
        //    /// <param name="prix"></param>
        [Theory]
        [InlineData(50.0)]
        [InlineData(100.0)]
        public void TestValidationPrix(double prix)
        {
            var consoleJeu = new ConsoleJeu
            {
                Prix = prix,
            };

            var context = new ValidationContext(consoleJeu);
            var results = new List<ValidationResult>();

            // TODO: Avec le prix 100.0 ça ne passe pas, c'est normal.
            //       Si tu veux le vérifier et que le teste passe, tu dois faire si prix == 100.0, alors Assert.False(estValide)
            var estValide = Validator.TryValidateObject(consoleJeu, context, results, true);
            Assert.True(estValide);
        }
    }
}

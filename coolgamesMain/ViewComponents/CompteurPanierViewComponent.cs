﻿using CoolGames.Models;
using Microsoft.AspNetCore.Mvc;

namespace CoolGames.ViewComponents
{
    public class CompteurPanierViewComponent : ViewComponent
    {
        private readonly IPanierRepository _panier;

        public CompteurPanierViewComponent(IPanierRepository panier)
        {
            _panier = panier;
        }
        /// <summary>
        /// Permet d'aller chercher le nombre d'article pour l'afficher dans la vue et dans la session courante
        /// </summary>
        /// <returns></returns>
        public IViewComponentResult Invoke()
        {
            string _panierSessionId = HttpContext.Session.GetString("PanierSessionId");
            int nbJeuxPanier = _panier.NombreArticlesPanier(_panierSessionId);
            return View(nbJeuxPanier);
        }
    }
}

﻿using System.ComponentModel.DataAnnotations;

namespace CoolGames.AttributsPersonnalises
{
    public class MotObligatoireAttribute : ValidationAttribute
    {
        private readonly string _name;
        private readonly string _secondName;
        private readonly string _thirdName;
        

        public MotObligatoireAttribute(string name, string secondName, string thirdName)
        {
            _name = name;
            _secondName = secondName;
            _thirdName = thirdName;
            
        }
        /// <summary>
        /// Méthode qui empêche d'écrire autre chose que Playstation 5, Xbox Series,Switch
        /// </summary>
        /// <param name="value"></param>
        /// <param name="validationContext"></param>
        /// <returns></returns>
        protected override ValidationResult? IsValid(object? value, ValidationContext validationContext)
        {
            if (value != null && value is string stringValue)
            {
                if (stringValue.Contains(_name) || stringValue.Contains(_secondName) || stringValue.Contains(_thirdName))
                {
                    return ValidationResult.Success;
                }
                else
                {
                    return new ValidationResult(ErrorMessage = "La plateforme doit être Playstation 5, Xbox Series,Nintento Switch");
                }
            }
            return ValidationResult.Success;
        }
    }
}

using CoolGames.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.AspNetCore.Identity;

var builder = WebApplication.CreateBuilder(args);
var connectionString = builder.Configuration.GetConnectionString("GamesDbContextConnection")
    ?? throw new InvalidOperationException("Connection string 'GamesDbContextConnection' not found.");

// Add services to the container.
//builder.Services.AddHttpContextAccessor();

builder.Services.AddControllersWithViews();
builder.Services.AddSession();
builder.Services.AddScoped<IJeuRepository, BDJeuRepository>();
builder.Services.AddScoped<IConsoleRepository, BDConsoleRepository>();
builder.Services.AddScoped<IPanierRepository, BDPanierRepository>();
builder.Services.AddScoped<BDPanierRepository>();

// TODO: tu avais un au dessus.
builder.Services.AddSession(options =>
{
    options.IdleTimeout = TimeSpan.FromMinutes(20);
});

builder.Services.AddDbContext<GamesDbContext>(options =>
{
    options.UseSqlServer(connectionString);
});

builder.Services.AddIdentity<IdentityUser, IdentityRole>(options => options.SignIn.RequireConfirmedAccount = true)
    .AddEntityFrameworkStores<GamesDbContext>().AddDefaultUI().AddDefaultTokenProviders();

var app = builder.Build();

//TODO: lis bien les prochaines lignes jusqu'� la ligne 62, tu comprendras les redondances dans ton code!

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Home/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

if (app.Environment.IsDevelopment())
{
    app.UseDeveloperExceptionPage();
}
else
{
    app.UseStatusCodePagesWithReExecute("/Erreurs/Statut{0}");
}
if (app.Environment.IsDevelopment())
{
    app.UseDeveloperExceptionPage();
}
else
{
    app.UseExceptionHandler("/ErreurGlobale");
}


app.UseHttpsRedirection();
app.UseStaticFiles();

app.UseRouting();

app.UseAuthentication();
app.UseAuthorization();
app.UseSession();
app.MapRazorPages();
app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Home}/{action=Index}/{id?}");

app.UseEndpoints(endpoints =>
{
    endpoints.MapControllers();
});
CoolGameBD.Seed(app);
app.Run();

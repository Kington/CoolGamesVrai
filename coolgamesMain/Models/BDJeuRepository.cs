﻿namespace CoolGames.Models
{
    public class BDJeuRepository : IJeuRepository
    {
        private readonly GamesDbContext _dbContext;

        public BDJeuRepository(GamesDbContext dbContext)
        {
            _dbContext = dbContext;
        }
        /// <summary>
        ///  Récupère une liste de tous les jeux disponibles dans la base de données, triés par nom
        /// </summary>
        public List<Jeu> ListeJeux
        {
            get
            {
                return _dbContext.Jeus.OrderBy(j => j.Nom).ToList();
                
            }
        }
        /// <summary>
        /// Ajoute un jeu à la base de données en utilisant la méthode Add() 
        /// du contexte de la base de données et l'enregistre dans la base de donnée
        /// </summary>
        /// <param name="jeu"></param>
        public void AjouterJeu(Jeu jeu)
        {
            _dbContext.Jeus.Add(jeu);
            _dbContext.SaveChanges();
        }
        /// <summary>
        /// Récupère un jeu spécifique en fonction de son identifiant depuis la base de données
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Jeu? GetJeu(int id)
        {
            return _dbContext.Jeus.FirstOrDefault(j => j.Id == id);
        }
        /// <summary>
        /// Met à jour les informations d'un jeu donné dans la base de données en utilisant la méthode Update() 
        /// du contexte de la base de données et l'enregistre dans la base de donnée
        /// </summary>
        /// <param name="jeu"></param>
        public void ModifierJeu(Jeu jeu)
        {
            _dbContext.Jeus.Update(jeu);
            _dbContext.SaveChanges();
        }
        /// <summary>
        /// Supprime un jeu spécifique de la base de données en le recherchant par son identifiant et l'enregistre dans la base de donnée
        /// </summary>
        /// <param name="id"></param>
        public void SupprimerJeu(int id)
        {
            var jeu = _dbContext.Jeus.FirstOrDefault(j => j.Id == id);
            if (jeu != null)
            {
                _dbContext.Jeus.Remove(jeu);
                _dbContext.SaveChanges();
            }
            
        }
        /// <summary>
        /// Effectue une recherche dans la base de données en filtrant les jeux dont le nom 
        /// contient la chaîne de caractères de recherche. Retourne une liste des jeux correspondants
        /// </summary>
        /// <param name="recherche"></param>
        /// <returns></returns>
        public List<Jeu> Recherche(string recherche)
        {
            recherche = recherche.ToLower();
            return _dbContext.Jeus.Where(j => j.Nom.ToLower().Contains(recherche)).ToList();
            
        }

       
    }
}

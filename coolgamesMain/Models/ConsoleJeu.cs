﻿using Microsoft.Build.Framework;
using System.ComponentModel.DataAnnotations;


namespace CoolGames.Models
{
    public class ConsoleJeu
    {
        public int Id { get; set; }
        public string Nom { get; set; }
        public DateTime DateSortie { get; set; }
        public string Stockage { get; set; }
        public List<Jeu> Jeux { get; set; }
        public string Compagnie { get; set; }
        public string? UrlImage { get; set; }
        [System.ComponentModel.DataAnnotations.Required]
        [Range(1, 99.99, ErrorMessage = "Le prix doit être entre 1$ et 99.99$")]
        public double Prix { get; set; }
        
    }
}

﻿using System.Diagnostics;

namespace CoolGames.Models
{
    public interface IPanierRepository
    {
        public List<Panier> ArticlePanier { get; }
        public void SetSessionId(string sessionId);
        public void AjouterAuPanier(Jeu jeu);
        public void SupprimerDuPanier(Jeu jeu);
        public void ViderPanier();
        public decimal MontantTotalPanier();
        public int NombreArticlesPanier(string PanierSessionId);
        public void InitialiserPanierId(string sessionId);
    }
}

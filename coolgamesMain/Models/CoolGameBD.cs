﻿using Microsoft.AspNetCore.Identity;

namespace CoolGames.Models
{
    public static class CoolGameBD
    {
        public static List<Jeu> _listeJeux = new List<Jeu>
        {
            new Jeu
            { 
                
                Nom = "Horizon Forbidden West",
                DateSortie = new DateTime(2022, 2, 18),
                PlatesFormes = "PlayStation 5",
                Developpeur = "Guerrilla Games",
                Editeur = "Sony Interactive Entertainment",
                Genres = "Action-RPG",
                Prix = 69.99,
                Quantite = 10,
              
                UrlImage="Horizon.png"

            },
            new Jeu
            {
                
                Nom = "Ratchet & Clank: Rift Apart",
                DateSortie = new DateTime(2021, 6, 11),
                PlatesFormes = "PlayStation 5",
                Developpeur = "Insomniac Games",
                Editeur = "Sony Interactive Entertainment",
                Genres = "Action-Platformer",
                Prix = 79.99,
                Quantite = 8,

                UrlImage="Ratchet.png"
            },
            new Jeu
            {
                
                Nom = "Returnal",
                DateSortie = new DateTime(2021, 4, 30),
                PlatesFormes = "PlayStation 5",
                Developpeur = "Housemarque",
                Editeur = "Sony Interactive Entertainment",
                Genres = "Action-Roguelike",
                Prix = 69.99,
                Quantite = 5,
              
                UrlImage="ReturnalUni.jpg"
            },
            new Jeu
            {
                Nom = "Demon's Souls",
                DateSortie = new DateTime(2020, 11, 12),
                PlatesFormes = "PlayStation 5",
                Developpeur = "Bluepoint Games",
                Editeur = "Sony Interactive Entertainment",
                Genres = "Action-RPG",
                Prix = 79.99,
                Quantite = 3,
               
                UrlImage="Demon.png"
            },
            new Jeu
            {
                Nom = "Spider-Man: Miles Morales",
                DateSortie = new DateTime(2020, 11, 12),
                PlatesFormes = "PlayStation 5",
                Developpeur = "Insomniac Games",
                Editeur = "Sony Interactive Entertainment",
                Genres = "Action-Adventure",
                Prix = 49.99,
                Quantite = 6,
                UrlImage = "Spider.png"
            },
            new Jeu{Nom = "Halo Infinite",DateSortie = new DateTime(2021, 12, 8),PlatesFormes = "Xbox Series X",Developpeur = "343 Industries",Editeur = "Microsoft Studios",Genres = "Shooter, FPS",Prix = 69.99,Quantite = 100, UrlImage="Halo.png"},
            new Jeu{Nom = "Forza Horizon 5",DateSortie = new DateTime(2021, 11, 9), PlatesFormes = "Xbox Series X",Developpeur = "Playground Games",Editeur = "Microsoft Studios",Genres = "Course, Simulation",Prix = 79.99,Quantite = 50,UrlImage="Forza.png"},
            new Jeu{Nom = "Fable",DateSortie = new DateTime(2024, 4, 23),PlatesFormes = "Xbox Series X",Developpeur = "Playground Games",Editeur = "Microsoft Studios",Genres = "Action-RPG",Prix = 89.99,Quantite = 30,UrlImage="Fable.png"},
            new Jeu{Nom = "Avowed",DateSortie = new DateTime(2023, 9, 15),PlatesFormes = "Xbox Series X",Developpeur = "Obsidian Entertainment",Editeur = "Microsoft Studios",Genres = "Action-RPG",Prix = 69.99,Quantite = 80, UrlImage="Avowed.png"},
            new Jeu{Nom = "Everwild",DateSortie = new DateTime(2023, 5, 11),PlatesFormes = "Xbox Series X",Developpeur = "Rare", Editeur = "Microsoft Studios",Genres = "Aventure, Exploration",Prix = 79.99,Quantite = 60, UrlImage="Everwild.png"},
            new Jeu{Nom = "The Legend of Zelda: Tears of the Kingdom",DateSortie = new DateTime(2023, 5, 13),PlatesFormes = "Nintendo Switch",Developpeur = "Nintendo EPD",Editeur = "Nintendo",Genres = "Action-Adventure",Prix = 59.99,Quantite = 100,UrlImage = "Zelda.jpg"},
            new Jeu{Nom = "Mario + Rabbids Sparks of Hope",DateSortie = new DateTime(2022, 10, 20),PlatesFormes = "Nintendo Switch",Developpeur = "Ubisoft Milan et Paris",Editeur = "Nintendo",Genres = "Action-Adventure",Prix = 79.99,Quantite = 100,UrlImage = "Mario.jpg"},
            new Jeu{Nom = "Animal Crossing: New Horizons",DateSortie = new DateTime(2020, 3, 20),PlatesFormes = "Nintendo Switch",Developpeur = "Nintendo EPD",Editeur = "Nintendo",Genres = "Simulation",Prix = 79.99,Quantite = 120,UrlImage = "Animal.jpg"},
            new Jeu{Nom = "Splatoon 3",DateSortie = new DateTime(2022, 9, 8),PlatesFormes = "Nintendo Switch",Developpeur = "Nintendo EPD",Editeur = "Nintendo",Genres = "Third-person shooter",Prix = 79.99,Quantite = 60,UrlImage = "Splatoon.jpg"},
            new Jeu{Nom = "Mario Kart 8 Deluxe",DateSortie = new DateTime(2017, 4, 28),PlatesFormes = "Nintendo Switch",Developpeur = "Nintendo EPD",Editeur = "Nintendo",Genres = "Racing",Prix = 68.99,Quantite = 70,UrlImage = "MarioKart.jpg"},
        };
        public static List<ConsoleJeu> _consoles = new List<ConsoleJeu>
        {
            new ConsoleJeu{Nom = "PlayStation 5",DateSortie = new DateTime(2020, 11, 12),Stockage = "825 Go SSD",Jeux = new List<Jeu> { },Compagnie = "Sony Interactive Entertainment", UrlImage="Playstation.jpg", Prix = 649.99},
            new ConsoleJeu{Nom = "Xbox Series X",DateSortie = new DateTime(2020, 11, 10),Stockage = "1 To SSD",Jeux = new List<Jeu> { },Compagnie = "Microsoft", UrlImage="Xbox.jpg",Prix = 599.99},
            new ConsoleJeu{Nom = "Nintendo Switch",DateSortie = new DateTime(2017, 3, 3),Stockage = "32 GB",Jeux = new List<Jeu> { },Compagnie = "Nintendo",UrlImage="Nintendo.jpg", Prix = 399.99}
        };
        /// <summary>
        /// Permet d'associer les jeux aux consoles respectives
        /// </summary>
        /// <param name="jeux"></param>
        /// <param name="consoles"></param>
        public static void AssocierJeuxAConsoles(List<Jeu> jeux, List<ConsoleJeu> consoles)
        {
            foreach (ConsoleJeu console in consoles)
            {
                List<Jeu> jeuxConsole = jeux.Where(j => j.PlatesFormes == console.Nom).ToList();
                console.Jeux.AddRange(jeuxConsole);
            }
        }

        /// <summary>
        /// Permet de créer 2 roles et deux utilisateurs par défauts
        /// </summary>
        /// <param name="application"></param>

        public static void Seed(IApplicationBuilder application)
        {
            GamesDbContext gamesDbContext = application.ApplicationServices.CreateScope().ServiceProvider.GetRequiredService<GamesDbContext>();

            if (!gamesDbContext.Roles.Any())
            {
                RoleManager<IdentityRole> roleManager = application.ApplicationServices.CreateScope().ServiceProvider.GetRequiredService<RoleManager<IdentityRole>>();

                // Ajouter les rôles par défaut
                IdentityRole role1 = new IdentityRole { Name = "Admin" };
                roleManager.CreateAsync(role1).GetAwaiter().GetResult();

                IdentityRole role2 = new IdentityRole { Name = "Utilisateur" };
                roleManager.CreateAsync(role2).GetAwaiter().GetResult();
            }

            if (!gamesDbContext.Users.Any())
            {
                UserManager<IdentityUser> userManager = application.ApplicationServices.CreateScope().ServiceProvider.GetRequiredService<UserManager<IdentityUser>>();

                // Ajouter les utilisateurs par défaut
                IdentityUser user1 = new IdentityUser
                {
                    Email = "admin@admin.com",
                    UserName = "admin@admin.com",
                    EmailConfirmed = true
                };
                userManager.CreateAsync(user1, "Password1-").GetAwaiter().GetResult();
                userManager.AddToRoleAsync(user1, "Admin").GetAwaiter().GetResult();

                IdentityUser user2 = new IdentityUser
                {
                    UserName = "user@user.com",
                    Email = "user@user.com",
                    EmailConfirmed = true
                };
                userManager.CreateAsync(user2, "Password1-").GetAwaiter().GetResult();
                userManager.AddToRoleAsync(user2, "Utilisateur").GetAwaiter().GetResult();
            }

            gamesDbContext.SaveChanges();

            if (!gamesDbContext.Consoles.Any())
            {
                gamesDbContext.Consoles.AddRange(_consoles);
            }
            gamesDbContext.SaveChanges();

            if (!gamesDbContext.Jeus.Any())
            {
                gamesDbContext.Jeus.AddRange(_listeJeux);
            }
            gamesDbContext.SaveChanges();

            AssocierJeuxAConsoles(_listeJeux, _consoles);
            gamesDbContext.SaveChanges();
        }
    
    }
}

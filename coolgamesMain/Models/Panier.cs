﻿namespace CoolGames.Models
{
    public class Panier
    {
        public int Id { get; set; }

        public int JeuId { get; set; }
        public Jeu Jeu { get; set; }
        public int Quantite { get; set; }
        public string PanierSessionId { get; set; }
    }
}

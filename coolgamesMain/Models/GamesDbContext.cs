﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace CoolGames.Models
{
    public class GamesDbContext : IdentityDbContext
    {
        public GamesDbContext(DbContextOptions<GamesDbContext> options) : base(options)
        {

        }

        public DbSet<ConsoleJeu> Consoles { get; set; }
        public DbSet<Jeu> Jeus { get; set; }
        public DbSet<Panier> Paniers { get; set; }
    }
}

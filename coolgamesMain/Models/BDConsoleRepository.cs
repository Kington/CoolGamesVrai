﻿using Microsoft.EntityFrameworkCore;

namespace CoolGames.Models
{
    public class BDConsoleRepository : IConsoleRepository
    {
        private readonly GamesDbContext _dbContext;

        public BDConsoleRepository(GamesDbContext dbContext)
        {
            _dbContext = dbContext;
        }
        /// <summary>
        /// Récupère une liste de consoles de jeu de la catégorie "Playstation 5" 
        /// avec leurs jeux associés depuis la base de données
        /// </summary>
        public List<ConsoleJeu> ListeConsolesPs5
        {
            get
            {
                return _dbContext.Consoles.Include(c => c.Jeux).Where(c => c.Nom == "Playstation 5").ToList();
            }
        }
        /// <summary>
        /// Récupère une liste de consoles de jeu de la catégorie "Xbox Series X" 
        /// avec leurs jeux associés depuis la base de données
        /// </summary>
        public List<ConsoleJeu> ListeConsolesXbox
        {
            get
            {
                return _dbContext.Consoles.Include(c => c.Jeux).Where(c => c.Nom == "Xbox Series X").ToList();
            }
        }
        /// <summary>
        /// Récupère une liste de consoles de jeu de la catégorie "Nintendo Switch"
        /// avec leurs jeux associés depuis la base de données
        /// </summary>
        public List<ConsoleJeu> ListeConsolesSwitch
        {
            get
            {
                return _dbContext.Consoles.Include(c => c.Jeux).Where(c => c.Nom == "Nintendo Switch").ToList();
            }
        }
        /// <summary>
        ///  Récupère une console de jeu spécifique en fonction de son identifiant depuis la base de données.
        ///  Retourne null si aucune console correspondante n'est trouvée
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ConsoleJeu? GetConsole(int id)
        {
            return _dbContext.Consoles.FirstOrDefault(j => j.Id == id);
        }
        /// <summary>
        /// Met à jour les informations d'une console de jeu donnée dans la base de données.
        /// et on l'enregistre dans la base de données
        /// </summary>
        /// <param name="console"></param>
        public void ModifierConsole(ConsoleJeu console)
        {
            _dbContext.Consoles.Update(console);
            _dbContext.SaveChanges();
        }
    }
}

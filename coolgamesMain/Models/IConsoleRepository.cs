﻿namespace CoolGames.Models
{
    public interface IConsoleRepository
    {
        public List<ConsoleJeu>  ListeConsolesPs5 { get; }
        public List<ConsoleJeu> ListeConsolesXbox { get; }
        public List<ConsoleJeu> ListeConsolesSwitch { get; }
        public ConsoleJeu? GetConsole(int id);
        public void ModifierConsole(ConsoleJeu console);

    }
}

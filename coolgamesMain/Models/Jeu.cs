﻿using CoolGames.AttributsPersonnalises;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using System.ComponentModel.DataAnnotations;

namespace CoolGames.Models
{
    public class Jeu
    {

        // TODO: Il manque la référence et la clé étrangère vers ConsoleJeu
        
        public int Id { get; set; }


        [Required(ErrorMessage = "Le nom de du jeu est requis")]
        public string Nom { get; set; }


        [Required(ErrorMessage = "La date de sortie du jeu est requise")]
        public DateTime DateSortie { get; set; }


        [Required(ErrorMessage = "La plateforme de sortie du jeu est requise")]
        [MotObligatoire("Playstation 5","Xbox Series","Nintendo Switch")]
        public string PlatesFormes { get; set; }


        [Required(ErrorMessage = "Le developpeur du jeu est requis")]
        [Display(Name = "Développeur")]
        public string Developpeur { get; set; }


        [Display(Name = "Éditeur")]
        public string Editeur { get; set; }

        [Required(ErrorMessage = "Le genre du jeu est requis")]
        public string Genres { get; set; }

        [Required(ErrorMessage = "Le prix du jeu est requis")]
        [RegularExpression(@"(^\d+(\,\d{1,2})?$)", ErrorMessage = "Le prix doit être en décimal")]
        [Range(1, 99.99, ErrorMessage = "Le prix doit être entre 1$ et 99.99$")]       
        public double Prix { get; set; }

        [Required(ErrorMessage = "La quantité du jeu est requise")]
        [Range(1, 200, ErrorMessage = "La quantité doit être entre 1 et 200")]
        [Display(Name = "Quantité")]
        [Remote("ValiderResultat", "Jeu", HttpMethod = "Post", AdditionalFields = "Quantite")]
        public int Quantite { get; set; }

        [BindNever]
        public byte[]? ImageData { get; set; }
        public string? UrlImage { get; set; }
    }
}

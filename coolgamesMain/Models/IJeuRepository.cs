﻿namespace CoolGames.Models
{
    public interface IJeuRepository
    {
        
        public List<Jeu> ListeJeux { get;}
        public Jeu? GetJeu(int id);
        public void AjouterJeu(Jeu jeu);
        public void SupprimerJeu(int id);
        public void ModifierJeu(Jeu jeu);
        public List<Jeu> Recherche(string recherche);
    }
}

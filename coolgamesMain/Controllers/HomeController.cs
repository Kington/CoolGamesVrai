﻿using CoolGames.Models;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;

namespace CoolGames.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public HomeController(ILogger<HomeController> logger, IHttpContextAccessor httpContextAccessor)
        {
            _logger = logger;
            _httpContextAccessor = httpContextAccessor;

        }
        /// <summary>
        /// Méthode pour afficher la page d'accueil
        /// </summary>
        /// <returns></returns>
        public IActionResult Index()
        {
            var requestCookies = _httpContextAccessor.HttpContext.Request.Cookies;
                var panier = requestCookies["Panier"];
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        
    }
}
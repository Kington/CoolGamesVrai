﻿using CoolGames.Models;
using CoolGames.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Schema;

namespace CoolGames.Controllers.Api
{
    // TODO: Je n'ai pas trouvé où tu appelles ton API côté client (?)

    [Route("api/Jeu")]
    [ApiController]
    public class MontantTotalController : ControllerBase
    {
        
        private readonly IJeuRepository _jeuRepository;

        public MontantTotalController(IJeuRepository jeuRepository)
        {
            
            _jeuRepository = jeuRepository;
        }
        [HttpGet]
        public IActionResult GetPrixTotaltJeu()
        {
            // TODO: Le nom dit que ça retourne le prix total, mais la requête fait autre chose.
            var mesJeux = _jeuRepository.ListeJeux.Where(j => j.Quantite < 75).ToList();
            

            string contenuFichier = System.IO.File.ReadAllText(@"wwwroot/SchemasJson/jeux-json-schema.json");
            JSchema schemaJsonJeux = JSchema.Parse(contenuFichier);

            // TODO: Modifie au moins les noms de variables.
            JArray gateauxJson = JArray.FromObject(mesJeux);

            // TODO: Bonne tentative de validation JSON! Prochain coup tu y arriveras :)
            //       Ta validation JSON ne fonctionne pas car ton schéma JSON définit les
            //       avec des types qui ne correspondent pas à ceux du modèle (retourné par la requête).
            bool gateauxJsonValide = gateauxJson.IsValid(schemaJsonJeux, out IList<string> erreurs);

            if (!gateauxJsonValide)
            {
                return BadRequest(string.Join(", ", erreurs));
            }
            return Ok(mesJeux);

        }
    }
}

﻿using CoolGames.Models;
using CoolGames.ViewModels;
using Microsoft.AspNetCore.Mvc;
using System;

namespace CoolGames.Controllers
{

    // TODO: Les utilisateurs partagent le même panier. Problème de gestion
    //       d’identifiants de session pour les utilisateurs.

    // TODO: Tu ne gère pas les autorisations ici?

    public class PanierController : Controller
    {
        private readonly IPanierRepository _panier;
        private readonly IHttpContextAccessor _httpContext;
        private readonly IJeuRepository _jeuxRepository;
        private readonly BDPanierRepository _bdPanier;

       
        /// <summary>
        /// Initialiser les variables et créer une nouvelle session avec un Panier
        /// </summary>
        /// <param name="panier"></param>
        /// <param name="httpContext"></param>
        /// <param name="jeuxRepository"></param>
        /// <param name="bdPanier"></param>
        public PanierController(IPanierRepository panier, IHttpContextAccessor httpContext, IJeuRepository jeuxRepository, BDPanierRepository bdPanier)
        {
            _panier = panier;
            _httpContext = httpContext;
            _jeuxRepository = jeuxRepository;
            _bdPanier = bdPanier;

            var session = _httpContext.HttpContext.Session;

            if (session.Keys.Contains("PanierSessionId"))
            {
                var sessionIdExistante = session.GetString("PanierSessionId");
                _panier.InitialiserPanierId(sessionIdExistante);
            }
            else
            {
                var nouvelleSessionId = Guid.NewGuid().ToString();

                session.SetString("PanierSessionId", nouvelleSessionId);

                _panier.InitialiserPanierId(nouvelleSessionId);
            }
        }
        /// <summary>
        /// Afficher le panier
        /// </summary>
        /// <returns></returns>
        public ViewResult AfficherPanier()
        {
            PanierViewModel panierViewModel = new PanierViewModel
            {
                ListeJeuxPanier = _panier.ArticlePanier,
                MontantTotal = _panier.MontantTotalPanier()
            };
            return View(panierViewModel);
        }
        /// <summary>
        /// Ajouter l'article dans le panier
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public RedirectToActionResult AjouterAuPanier(int id)
        {
            Jeu jeu = _jeuxRepository.GetJeu(id);

            if (jeu != null)
            {
                _panier.AjouterAuPanier(jeu);
            }           
            return RedirectToAction(nameof(AfficherPanier));
        }
        /// <summary>
        /// Supprimer le jeu du panier
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public RedirectToActionResult SupprimerDuPanier(int id)
        {
            Jeu jeu = _jeuxRepository.GetJeu(id);

            if (jeu != null)
            {
                _panier.SupprimerDuPanier(jeu);
            }
            return RedirectToAction(nameof(AfficherPanier));
        }
        /// <summary>
        /// Vider le panier au complet
        /// </summary>
        /// <returns></returns>
        public RedirectToActionResult ViderPanier()
        {
            _panier.ViderPanier();
            return RedirectToAction(nameof(AfficherPanier));
        }

        public void SavePanier()
        {
            // TODO: - Je ne vois pas où tu utilises ce cookie
            //       - Utilise l'option httpOnly pour le rendre inaccessible via javascript (sécurité)
            var selections = _bdPanier.ArticlePanier;
            var selectionsString = string.Join(",", selections);

            var reponse = _httpContext.HttpContext.Response.Cookies;
            reponse.Append("Panier", selectionsString);
        }
    }
}

﻿using CoolGames.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using X.PagedList;

namespace CoolGames.Controllers
{
    public class JeuController : Controller
    {
        private readonly IJeuRepository _jeuRepository;


        public JeuController(IJeuRepository jeuRepository)
        {
            _jeuRepository = jeuRepository;
        }
        // TODO: Dans tes descriptions de méthodes, tu ne décris pas les paramètres.

        /// <summary>
        /// Méthode pour afficher la liste des jeux. De plus, il est possible de faire une recherche et changer de page
        /// </summary>
        /// <param name="recherche"></param>
        /// <param name="page"></param>
        /// <returns></returns>
        public ActionResult Liste(string recherche, int? page)
        {
            ViewBag.recherche = recherche;
            if (recherche == null)
            {
                var listeJeux = _jeuRepository.ListeJeux;
                int taillePage = 9;
                int numeroPage = (page ?? 1);
                return View(listeJeux.ToPagedList(numeroPage, taillePage));
            }
            else
            {
                var jeuxRecherches = _jeuRepository.Recherche(recherche);
                int taillePage = 9;
                int numeroPage = page ?? 1;
                var listeJeuxPaginee = jeuxRecherches.ToPagedList(numeroPage, taillePage);
                return View(listeJeuxPaginee);
            }
        }
        /// <summary>
        /// Voir les détails d'un jeu
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ViewResult Details(int id)
        {
            Jeu jeu = _jeuRepository.GetJeu(id);
            return View(jeu);


        }
        /// <summary>
        /// Supprimer un jeu. Seulement un admin peut le faire
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Authorize(Roles = "Admin")]
        public RedirectToActionResult Supprimer(int id)
        {
            _jeuRepository.SupprimerJeu(id);
            return RedirectToAction("Liste", _jeuRepository);
        }
        /// <summary>
        /// Afficher la page de modification d'un jeu. Seulement un admin peut le faire
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Authorize(Roles = "Admin")]
        [HttpGet]
        public ViewResult Modifier(int id)
        {
            Jeu jeu = _jeuRepository.GetJeu(id);
            return View(jeu);
        }
        /// <summary>
        /// Permet de modifier un jeu. Il y a aussi un ajout avec le téléversement d'image. Seulement un admin peut le faire
        /// </summary>
        /// <param name="jeu"></param>
        /// <param name="imageFile"></param>
        /// <returns></returns>
        [Authorize(Roles = "Admin")]
        [HttpPost]
        public ActionResult Modifier(Jeu jeu, IFormFile imageFile)
        {
            // TODO: quelque chose m'empêche de faire la modification mais je ne vois pas de message d'erreur.

            if (ModelState.IsValid)
            {
                if (imageFile != null && imageFile.Length > 0)
                {
                    using (var memoryStream = new MemoryStream())
                    {
                        imageFile.CopyTo(memoryStream);
                        jeu.ImageData = memoryStream.ToArray();
                    }

                }
                _jeuRepository.ModifierJeu(jeu);
                return RedirectToAction(nameof(Liste), new { id = jeu.Id });
            }
            else
            {
                return View();
            }
        }
        /// <summary>
        /// Afficher la création d'un jeu
        /// </summary>
        /// <returns></returns>
        [Authorize(Roles = "Admin")]
        [HttpGet]
        public ViewResult Ajouter()
        {
            return View();
        }
        /// <summary>
        /// Creer le jeu. Seulement un admin peut le faire
        /// </summary>
        /// <param name="jeu"></param>
        /// <param name="imageFile"></param>
        /// <returns></returns>
        [Authorize(Roles = "Admin")]
        [HttpPost]
        public ActionResult Ajouter(Jeu jeu, IFormFile imageFile)
        {
            if (ModelState.IsValid)
            {
                if (imageFile != null && imageFile.Length > 0)
                {
                    // TODO: Ça à l'air que tu stocke l'image dans la BD,
                    //        tu pouvais juste la stocker sur le serveur dans un fichier séparé.
                    using (var memoryStream = new MemoryStream())
                    {
                        imageFile.CopyTo(memoryStream);
                        jeu.ImageData = memoryStream.ToArray();
                    }

                }
                _jeuRepository.AjouterJeu(jeu);
                return RedirectToAction(nameof(Liste));
            }
            else
            {
                return View();

            }
        }
        /// <summary>
        /// Permet de ne pas laisser mettre la même quantité 
        /// </summary>
        /// <param name="quantite"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult ValiderResultat(int quantite)
        {
            // TODO: Je ne suis pas sûre de comprendre le sens de cette validation.
            //       Mais techniquement elle fonctionne.
            var resultat = _jeuRepository.ListeJeux.Any(j => j.Quantite == quantite);

            if (resultat)
            {
                return Json($"La quantité {quantite} existe déjà");
            }
            else
            {
                return Json(true);
            }
        }
    }
}

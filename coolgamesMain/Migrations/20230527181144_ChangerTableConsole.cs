﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace CoolGames.Migrations
{
    /// <inheritdoc />
    public partial class ChangerTableConsole : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Jeus_Consoles_ConsoleId",
                table: "Jeus");

            migrationBuilder.RenameColumn(
                name: "ConsoleId",
                table: "Jeus",
                newName: "ConsoleJeuId");

            migrationBuilder.RenameIndex(
                name: "IX_Jeus_ConsoleId",
                table: "Jeus",
                newName: "IX_Jeus_ConsoleJeuId");

            migrationBuilder.AddForeignKey(
                name: "FK_Jeus_Consoles_ConsoleJeuId",
                table: "Jeus",
                column: "ConsoleJeuId",
                principalTable: "Consoles",
                principalColumn: "Id");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Jeus_Consoles_ConsoleJeuId",
                table: "Jeus");

            migrationBuilder.RenameColumn(
                name: "ConsoleJeuId",
                table: "Jeus",
                newName: "ConsoleId");

            migrationBuilder.RenameIndex(
                name: "IX_Jeus_ConsoleJeuId",
                table: "Jeus",
                newName: "IX_Jeus_ConsoleId");

            migrationBuilder.AddForeignKey(
                name: "FK_Jeus_Consoles_ConsoleId",
                table: "Jeus",
                column: "ConsoleId",
                principalTable: "Consoles",
                principalColumn: "Id");
        }
    }
}

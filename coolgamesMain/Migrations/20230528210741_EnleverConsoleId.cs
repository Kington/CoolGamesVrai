﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace CoolGames.Migrations
{
    /// <inheritdoc />
    public partial class EnleverConsoleId : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
            name: "FK_Paniers_Consoles_ConsoleId",
            table: "Paniers");

            migrationBuilder.DropIndex(
                name: "IX_Paniers_ConsoleId",
                table: "Paniers");

            migrationBuilder.DropColumn(
                name: "ConsoleId",
                table: "Paniers");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
            name: "ConsoleId",
            table: "Paniers",
            nullable: false,
            defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Paniers_ConsoleId",
                table: "Paniers",
                column: "ConsoleId");

            migrationBuilder.AddForeignKey(
                name: "FK_Paniers_Consoles_ConsoleId",
                table: "Paniers",
                column: "ConsoleId",
                principalTable: "Consoles",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}

﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace CoolGames.Migrations
{
    /// <inheritdoc />
    public partial class ModifPanierJeu : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Jeus_Consoles_ConsoleId",
                table: "Jeus");

            

            migrationBuilder.AlterColumn<int>(
                name: "ConsoleId",
                table: "Jeus",
                type: "int",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AddForeignKey(
                name: "FK_Jeus_Consoles_ConsoleId",
                table: "Jeus",
                column: "ConsoleId",
                principalTable: "Consoles",
                principalColumn: "Id");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Jeus_Consoles_ConsoleId",
                table: "Jeus");

            

            migrationBuilder.AlterColumn<int>(
                name: "ConsoleId",
                table: "Jeus",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            

            migrationBuilder.AddForeignKey(
                name: "FK_Jeus_Consoles_ConsoleId",
                table: "Jeus",
                column: "ConsoleId",
                principalTable: "Consoles",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            
        }
    }
}
